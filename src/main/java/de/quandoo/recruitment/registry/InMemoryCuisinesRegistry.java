package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @CuisineRegistry implementation
 */
public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private Map<Cuisine, ConcurrentHashMap<Customer,Customer>> cuisineRegistry = new ConcurrentHashMap<>();
    private Map<Customer, ConcurrentHashMap<Cuisine,Cuisine>> customerRegistry = new ConcurrentHashMap<>();

    /** {@inheritDoc}*/
    @Override
    public void register(final Customer userId, final Cuisine cuisine) {
        if (userId != null && cuisine != null) {
            cuisineRegistry.computeIfAbsent(cuisine, k -> new ConcurrentHashMap<>()).put(userId, userId);
            customerRegistry.computeIfAbsent(userId, k -> new ConcurrentHashMap<>()).put(cuisine, cuisine);
        }
    }

    /** {@inheritDoc}*/
    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        if (cuisine != null) {
            return Optional.ofNullable(cuisineRegistry.get(cuisine))
                    .map( e -> new ArrayList<>(e.keySet()))
                    .orElse(null);
        } else
            return null;
    }

    /** {@inheritDoc}*/
    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        if (customer != null) {
            return Optional.ofNullable(customerRegistry.get(customer))
                    .map(e -> new ArrayList<>(e.keySet()))
                    .orElse(null);
        } else
            return null; // Empty list ?
    }

    /** {@inheritDoc}*/
    @Override
    public List<Cuisine> topCuisines(final int n) {
        if (n > 0) {
            return cuisineRegistry.entrySet()
                    .stream()
                    .sorted(Collections.reverseOrder(
                            Comparator.comparing(Map.Entry::getValue,
                                    Comparator.comparing(Map::keySet,
                                            Comparator.comparing(Set::size))))
                    )
                    .limit(n)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());
        } else
            return Collections.emptyList();
    }
}
