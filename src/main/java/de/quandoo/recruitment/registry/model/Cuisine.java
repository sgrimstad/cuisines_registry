package de.quandoo.recruitment.registry.model;

import java.util.Objects;

/**
 * Cuisine entity
 */
public class Cuisine {

    private final String name;

    public Cuisine(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /** {@inheritDoc}*/
    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;
        if (this == o)
            return true;
        if (Cuisine.class.isAssignableFrom(o.getClass())) {
            return Objects.equals(name, Cuisine.class.cast(o).name);
        } else
            return false;
    }

    /** {@inheritDoc}*/
    @Override
    public int hashCode() {
        return name.hashCode();
    }

}
