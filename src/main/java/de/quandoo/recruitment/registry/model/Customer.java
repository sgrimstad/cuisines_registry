package de.quandoo.recruitment.registry.model;

import java.util.Objects;

/**
 * Customer entity
 */
public class Customer {
    private final String uuid;

    public Customer(final String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    /** {@inheritDoc}*/
    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;
        if (this == o)
            return true;
        if (Customer.class.isAssignableFrom(o.getClass())) {
            return Objects.equals(uuid, Customer.class.cast(o).uuid);
        } else
            return false;
    }

    /** {@inheritDoc}*/
    @Override
    public int hashCode() {
        return uuid.hashCode();
    }
}
