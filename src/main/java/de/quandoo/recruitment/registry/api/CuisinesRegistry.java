package de.quandoo.recruitment.registry.api;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import java.util.List;

/**
 * Registry tha enable querying cuisine to customer and customer to cuisine mappings
 */
public interface CuisinesRegistry {

    /**
     * Register mapping, null-tolerable, not storing nulls
     * @param customer Customer to register cuisine
     * @param cuisine Cuisine to register
     */
    void register(Customer customer, Cuisine cuisine);

    /**
     * Obtain list of cuisine associated with customer
     * null-tolerable
     * @param customer Customer of interest
     * @return Copy of registered cuisines, null if no customer registered
     * (could be reconsidered in favour of empty list).
     */
    List<Cuisine> customerCuisines(Customer customer);

    /**
     * Obtain list of cuisines with most customers registered.
     * @param n limit to return
     * @return top n list, empty list in case of invalid parameter or empty registry
     */
    List<Cuisine> topCuisines(int n);

    /**
     * Obtain list of customers associated with cuisine
     * null-tolerable
     * @param cuisine Cuisine of interest
     * @return Copy of registered customers, null if no cuisine is not registered
     * (could be reconsidered in favour of empty list).
     */
    List<Customer> cuisineCustomers(Cuisine cuisine);
}
