package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ConcurrencyTest {
    private InMemoryCuisinesRegistry cuisinesRegistry;

    private static final List<Cuisine> CUISINES = Arrays.asList(
            new Cuisine("french"),
            new Cuisine("german"),
            new Cuisine("italian"),
            new Cuisine("russian"),
            new Cuisine("norwegian")
    );

    @Before
    public void init() {
        cuisinesRegistry = new InMemoryCuisinesRegistry();
    }

    @Test
    public void concurrentTest() {
        ThreadPoolExecutor executor = (ThreadPoolExecutor) (Executors.newFixedThreadPool(30));
        for (int i = 0; i < 3; i++) {
            final int idx = i;
            for (Cuisine cur : CUISINES) {
                executor.execute(new Runnable() {
                    final Thread main = Thread.currentThread();

                    @Override
                    public void run() {
                        try {
                            long counter = 0L;
                            while (true) {
                                cuisinesRegistry.register(new Customer(cur.getName() + idx + counter), cur);
                                counter++;
                            }
                        } catch (Exception e) {
                            System.err.println(String.format("registering with registry=%s, %s, idx=%d",
                                    cuisinesRegistry,
                                    cur,
                                    idx));
                            e.printStackTrace(System.err);
                            main.interrupt();
                        }
                    }
                });
                executor.execute(new Runnable() {

                    final Thread main = Thread.currentThread();

                    @Override
                    public void run() {
                        try {
                            cuisinesRegistry.cuisineCustomers(cur);
                            cuisinesRegistry.topCuisines(10);
                        } catch (Exception e) {
                            e.printStackTrace(System.err);
                            main.interrupt();
                        }

                    }
                });
            }
        }
        System.out.println("All threads are launched");
        for (int i = 0; i < 5; i++) {
            try {
                Thread.sleep(1_000);
                System.out.println("Tick "+i);
            } catch (InterruptedException e) {
                Assert.fail();
            }
        }
        System.out.println("Shutting threads down");
        executor.shutdownNow();
        try {
            Thread.sleep(15_000);
            System.out.println("Shutting threads timeout is over");
        } catch (InterruptedException e) {
            Assert.fail();
        }
    }
}
