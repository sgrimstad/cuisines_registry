package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ExtraInMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry;

    private static final Cuisine FRENCH = new Cuisine("french");
    private static final Cuisine GERMAN = new Cuisine("german");
    private static final Cuisine ITALIAN = new Cuisine("italian");

    private static final Customer CUST_1 = new Customer("1");
    private static final Customer CUST_2 = new Customer("2");
    private static final Customer CUST_3 = new Customer("3");

    @Before
    public void init() {
        cuisinesRegistry = new InMemoryCuisinesRegistry();

        cuisinesRegistry.register(CUST_3, FRENCH);
        cuisinesRegistry.register(CUST_3, GERMAN);
        cuisinesRegistry.register(CUST_3, ITALIAN);
        cuisinesRegistry.register(CUST_2, FRENCH);
        cuisinesRegistry.register(CUST_2, GERMAN);
        cuisinesRegistry.register(CUST_1, GERMAN);
    }

    @After
    public void after() {
        cuisinesRegistry = null;
    }

    @Test
    public void findByCuisine() {
        assertNull(cuisinesRegistry.cuisineCustomers(null));
        assertNull(cuisinesRegistry.cuisineCustomers(new Cuisine("None")));
        assertEquals(cuisinesRegistry.cuisineCustomers(GERMAN).size(), 3);
        assertTrue(cuisinesRegistry.cuisineCustomers(GERMAN).containsAll(Arrays.asList(CUST_1, CUST_2, CUST_3)));
        assertEquals(cuisinesRegistry.cuisineCustomers(FRENCH).size(), 2);
        assertTrue(cuisinesRegistry.cuisineCustomers(FRENCH).containsAll(Arrays.asList(CUST_2, CUST_3)));
        assertEquals(cuisinesRegistry.cuisineCustomers(ITALIAN).size(), 1);
        assertTrue(cuisinesRegistry.cuisineCustomers(ITALIAN).containsAll(Arrays.asList(CUST_3)));
    }

    @Test
    public void findByCustomer() {
        assertNull(cuisinesRegistry.customerCuisines(null));
        assertNull(cuisinesRegistry.customerCuisines(new Customer("-1")));
        assertEquals(3, cuisinesRegistry.customerCuisines(CUST_3).size());
        assertTrue(cuisinesRegistry.customerCuisines(CUST_3).containsAll(Arrays.asList(GERMAN, FRENCH, ITALIAN)));
        assertEquals(2, cuisinesRegistry.customerCuisines(CUST_2).size());
        assertTrue(cuisinesRegistry.customerCuisines(CUST_2).containsAll(Arrays.asList(GERMAN, FRENCH)));
        assertEquals(1, cuisinesRegistry.customerCuisines(CUST_1).size());
        assertTrue(cuisinesRegistry.customerCuisines(CUST_1).containsAll(Arrays.asList(GERMAN)));
    }

    @Test
    public void topN() {
        assertEquals(0, cuisinesRegistry.topCuisines(-1).size());
        List<Cuisine> topOne = cuisinesRegistry.topCuisines(1);
        assertEquals(1, topOne.size());
        assertEquals( GERMAN, topOne.get(0));
        List<Cuisine> topTwo = cuisinesRegistry.topCuisines(2);
        assertEquals(2, topTwo.size());
        assertEquals( GERMAN, topTwo.get(0));
        assertEquals( FRENCH, topTwo.get(1));
        List<Cuisine> topThree = cuisinesRegistry.topCuisines(3);
        assertEquals(3, topThree.size());
        assertEquals( GERMAN, topThree.get(0));
        assertEquals( FRENCH, topThree.get(1));
        assertEquals( ITALIAN, topThree.get(2));
        List<Cuisine> topFour = cuisinesRegistry.topCuisines(4);
        assertEquals(3, topFour.size());
        assertEquals( GERMAN, topFour.get(0));
        assertEquals( FRENCH, topFour.get(1));
        assertEquals( ITALIAN, topFour.get(2));
    }
}
